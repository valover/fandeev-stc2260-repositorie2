import java.util.Arrays;

public class MovingInArray {

    public static void main(String[] args) {
        int[] array = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        System.out.println(Arrays.toString(array));
        sortArray(array);
        System.out.println(Arrays.toString(array));
    }

    public static void sortArray(int[] array){
        int[] sortArray = new int[array.length];
        int nz = 0;
        for (int i = 0; i < array.length; i++){
            if (array[i] != 0){
                sortArray[nz] = array[i];
                nz++;
            }
        }
        for (int i = 0; i < array.length; i++){
            array[i] = sortArray[i];
        }
    }
}
